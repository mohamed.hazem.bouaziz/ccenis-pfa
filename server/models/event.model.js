const mongoose = require('mongoose');


var eventSchema = new mongoose.Schema({

    club: {
        type: String,
        
    },
    nom: {
        type: String,
    },
  
    started_date: {
        type: Date
       
    },
    end_date: {
        type: Date
       
    },
   
    form: {
        type: String,
       
    },
    contact: {
        type: String,
       
    },
    site_web: {
        type: String,
       
    },
    facebook:{
        type:String
    },
    description: {
        type: String,
       
    },
    logo: {
        type: String,
       
    },
   
    saltSecret: String
});
mongoose.model('Event', eventSchema);