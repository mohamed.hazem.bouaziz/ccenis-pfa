const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var emploiSchema = new Schema({
    groupe: {
        type: String,
        required: true,   
    },
    matiere: {
        type: String,
        required: true
    },
    expireAfterSeconds:{
        type:Number
    },
    anneescolaire: {
        type: String,
        required: true
    },
    jour: {
        type: String,
        required: true
    },
    horaire: {
        type: String,
        required: true
    },
    prof: {
        type: String,
        required: true
    },
    salle: {
        type: String,
        required: true
    },
    
},  {
    timestamps: true
});

var Emplois = mongoose.model('Emploi', emploiSchema);

module.exports = Emplois;