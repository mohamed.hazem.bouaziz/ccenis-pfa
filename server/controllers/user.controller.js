const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');
const path = require('path');
const { forEach } = require('lodash');
multer = require('multer')
bodyParser = require('body-parser');
const User = mongoose.model('User');
const Event = mongoose.model('Event');
const Emploi = require('../models/emploi');

const Offre = mongoose.model('Offre');

const Recommandation = mongoose.model('Recommandation');

module.exports.addEvent = (req, res, next) => {
    var event = new Event();
    event.nom = req.body.nom;
    event.description = req.body.description
    event.logo = req.body.logo;
    event.started_date = req.body.started_date;
    event.end_date = req.body.end_date;
    event.facebook = req.body.facebook
    event.site_web = req.body.site_web;
    event.form = req.body.form;
    event.club = req.body.event;
    event.contact = req.body.contact

    event.save((err, doc) => {
        if (!err)
            res.send(doc);
        else {
            if (err.code == 11000)
                res.status(422).send(['error']);
            else
                return next(err);
        }

    });
}
module.exports.register = (req, res, next) => {
    var user = new User();
    user.username = req.body.username;
    user.email = req.body.email;
    user.password = req.body.password;
    user.role = req.body.role;
    user.cin = req.body.cin;
    user.image = req.body.image;
    user.nom = req.body.nom;
    user.prenom = req.body.prenom;
    user.skills = req.body.skills;
    user.section = req.body.section
    user.nomSociété = req.body.nomSociété;
    user.sexe = req.body.sexe;
    user.adresse = req.body.adresse;
    user.num_tel = req.body.num_tel;
    user.site_web = req.body.site_web;
    user.specialité = req.body.specialité;
    user.recommandations = req.body.recommandations


    user.save((err, doc) => {
        if (!err)
            res.send(doc);
        else {
            if (err.code == 11000)
                res.status(422).send(['Duplicate email adrress found.']);
            else
                return next(err);
        }

    });
}

module.exports.authenticate = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {
        // error from passport middleware
        if (err)
            return res.status(400).json(err);
        // registered user
        else if (user) {
            return res.status(200).json({ "token": user.generateJwt() });
        }
        // unknown user or wrong password
        else
            return res.status(404).json(info);
    })(req, res);

}
module.exports.addRecommandation = (req, res, next) => {
    var recommandation = new Recommandation()
    recommandation.prof = req.body.prof
    recommandation.name = req.body.name
    recommandation.rating = req.body.rating
    recommandation.description = req.body.description
    recommandation.student = req.params._id
    recommandation.save((err, doc) => {
        if (!err) {
            User.findOne({ _id: req.params._id },
                (err, user) => {
                    if (!user) {

                    }
                    else {

                        User.findByIdAndUpdate(req.params._id, { $push: { recommandations: recommandation } }, (err, doc) => {
                            if (err) return res.send(err.message)
                            if (doc) return res.send(doc);
                        })
                    }
                }
            );
        }
        else {

            return next(err);
        }

    });

}


module.exports.userProfile = (req, res, next) => {
    User.findOne({ _id: req._id }).populate('recommandations').populate('offres')
        .exec(function (err, user) {
            if (!user) {
                console.log(req._id)

                return res.status(404).json({ status: false, message: 'User record not found.', err: err, id: req._id });
            }
            else {

                return res.status(200).json({ status: true, user: _.pick(user, ['_id', 'offres', "recommandations", 'image', 'username', 'email', 'role', 'skills', 'cin', "prenom", "nom", "sexe", "adresse", "num_tel", "section"]) });
            }
        }
        );
}

module.exports.updateUser = (req, res, next) => {
    const id = req.params._id;
    const newUserData = req.body;
    User.findByIdAndUpdate(id, { $set: newUserData }, (err, doc) => {
        if (err) return res.send(err.message)
        if (doc) return res.send(doc);
    })
}

const PATH = './';

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, './uploads/'))
    },
    filename: (req, file, cb) => {
        cb(null, file.user.username)
    }

});
let upload = multer({
    storage: storage
});

module.exports.getUsername = (req, res, next) => {
    User.findOne({ _id: req._id },
        (err, user) => {

            console.log(req._id)
            return res.status(200).json({ status: true, user: _.pick(user, ['_id', 'image', 'username', 'email', 'role', 'skills', 'cin', "prenom", "nom", "sexe", "adresse", "num_tel"]) });
        }
    );
}
module.exports.uploadImage = (req, res) => {
    if (!req.file) {
        console.log("No file is available!");
        return res.send({
            success: false
        });

    } else {
        console.log('File is available!');
        return res.send({
            success: true
        })
    }

}

module.exports.userDetails = (req, res, next) => {
    User.findOne({ _id: req.params._id }).populate('recommandations')
        .exec(function (err, user) {
            if (!user) {

                return res.status(404).json({ status: false, message: 'User record not found.', err: err, id: req._id });
            }
            else {
                return res.status(200).json({ status: true, user: _.pick(user, ['_id', "recommandations", 'image', 'username', 'email', 'role', 'skills', 'cin', "prenom", "nom", "sexe", "adresse", "num_tel", "recommandations"]) });
            }
        }
        );
}
module.exports.eventDetails = (req, res, next) => {
    Event.findOne({ _id: req.params._id },
        (err, event) => {
            if (!event) {

                return res.status(404).json({ status: false, message: 'Event record not found.', err: err, id: req.params._id });
            }
            else {
                return res.status(200).json({ status: true, event: _.pick(event, ['_id', 'facebook', 'form', 'nom', 'contact', 'club', 'started_date', 'end_date', 'description', "contact", 'site_web']) });
            }
        }
    );
}
module.exports.getEvents = (req, res, next) => {
    Event.find({}, function (err, events) {
        var eventMap = []
        events.forEach(function (event) {
            eventMap.push(event)
        })
        res.send(eventMap)
    })
}
module.exports.getUsers = (req, res, next) => {
    User.find({}).populate('offres')
        .exec(function (err, users) {

            var userMap = [];

            users.forEach(function (user) {

                userMap.push(user);
            });

            res.send(userMap);

        });
}
module.exports.addOffre = (req, res, next) => {
    var offre = new Offre();
    offre.name = req.body.name;
    offre.description = req.body.description
    offre.skills = req.body.skills
    offre.save((err, doc) => {
        if (!err) {
            User.findByIdAndUpdate(req.body.indus, { $push: { offres: offre } }, (err, doc) => {
                if (err) return res.send(err.message)
                if (doc) return res.send(doc);
            })
        }
        else {

            return next(err);
        }

    });

}