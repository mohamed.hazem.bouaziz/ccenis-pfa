const express = require('express');
const router = express.Router();
const ctrlUser = require('../controllers/user.controller');
const User = require('../models/user.model')
const jwtHelper = require('../config/jwtHelper');
router.get('/', (req, res, next) => {
  res.send("header");
});
const PATH = '../App/src/assets/uploads';
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, PATH);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname)
  }
});
let upload = multer({
  storage: storage
});

router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile', jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.post('/addEvent', ctrlUser.addEvent);
router.get('/getevents', ctrlUser.getEvents);
router.post('/addOffer',ctrlUser.addOffre);

router.get('/getusers', ctrlUser.getUsers);
router.get('/userDetails/:_id', ctrlUser.userDetails, jwtHelper.verifyJwtToken);
router.get('/eventDetails/:_id', ctrlUser.eventDetails);
router.post('/addRecommandation/:_id', ctrlUser.addRecommandation);

router.put('/update/:_id', ctrlUser.updateUser)
router.post('/uploadimage', upload.single('image'), ctrlUser.uploadImage)
module.exports = router;



