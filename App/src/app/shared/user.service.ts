import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from './user.model';
import { Role } from './role.model';
import { Observable } from 'rxjs';
import { Recommandation } from './recommandation.model';
import { Emploi } from './emploi.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectedUser: User = {
    username: '',
    email: '',
    password: '',
    role: '',
    section: '',
    image: ''
  };
  selectedRecommandation: Recommandation = {
    prof: '',
    name: '',
    description: '',
    rating: new Int16Array
  };

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(public http: HttpClient) { }

  //HttpMethods

  postUser(user: User) {
    return this.http.post(environment.apiBasedUrl + '/register', user, this.noAuthHeader);
  }

  login(authCredentials) {
    return this.http.post(environment.apiBasedUrl + '/authenticate', authCredentials, this.noAuthHeader);
  }

  getUserProfile() {
    return this.http.get(environment.apiBasedUrl + '/userProfile');
  }
  getUser(_id: string): Observable<User> {
    return this.http.get<User>(environment.apiBasedUrl + '/userDetails/' + _id)
  }
  //Helper Methods
  getUsers() {
    return this.http.get<User[]>(environment.apiBasedUrl + '/getusers')

  }
  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }
 
  getUserPayload() {
    var token = this.getToken();
    if (token) {
      var userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    }
    else
      return null;
  }

  isLoggedIn() {
    var userPayload = this.getUserPayload();
    if (userPayload)
      return userPayload.exp > Date.now() / 1000;
    else
      return false;
  }
  updateUser(user: User, _id: String) {
    return this.http.put(environment.apiBasedUrl + '/update/' + _id, user);

  }
  addRecommandation(recommandation:Recommandation,_id: String){
    return this.http.post(environment.apiBasedUrl+'/addRecommandation/'+_id,recommandation)
  }
  getEmploi(){
    return this.http.get<Emploi[]>("http://localhost:3000/emploi/view/")
  }
  getSeance(){
    return this.http.get<String>("http://localhost:3000/emploi/notify/")
  }

}
