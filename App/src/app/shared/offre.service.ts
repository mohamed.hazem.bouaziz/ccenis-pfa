import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Offre } from './offre.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OffreService {
  constructor(public http: HttpClient) { 

  }
selectedOffer:Offre ={
  indus:'',
  name:'',
  description:'',
  skills:[]
}

  postOffre(offre:Offre){
    return this.http.post<Offre>(environment.apiBasedUrl+"/addOffer",offre)
  }
}
