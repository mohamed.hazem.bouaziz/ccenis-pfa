
export class User {
    username: string;
    email: string;
    password: string;
    role: string;
    image: string;
    section: string

}
