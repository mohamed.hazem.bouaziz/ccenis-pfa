import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/user.service';


@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.component.html',
  styleUrls: ['./annuaire.component.scss']
})

export class AnnuaireComponent {
  users: User[];

  searchText;
  constructor(private userService: UserService) {


  }
  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => this.users = users)
  }
}