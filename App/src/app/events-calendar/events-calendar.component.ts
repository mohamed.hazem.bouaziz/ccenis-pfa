import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';
import { Event } from '../shared/event.model';
@Component({
  selector: 'app-events-calendar',
  templateUrl: './events-calendar.component.html',
  styleUrls: ['./events-calendar.component.scss']
})
export class EventsCalendarComponent {
  constructor(public eventService: EventService) { }

  events: Event[]

  ngOnInit() {
    this.eventService.getEvents()
      .subscribe(events => this.events = events)
  }
  getDescription(string) {
    return string.substr(0, 20)
  }
}

