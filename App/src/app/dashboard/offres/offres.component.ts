import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/user.model';

@Component({
  selector: 'app-offres',
  templateUrl: './offres.component.html',
  styleUrls: ['./offres.component.scss']
})
export class OffresComponent implements OnInit {
  users:User[]
  constructor(public userService: UserService, public router: Router) { }
  ngOnInit(): void {
    this.userService.getUsers()
    .subscribe(users => this.users = users)
  }
  getImageName(user:User) {
    return user.username + '.' + user.image.split('.')[1];
  }
}
