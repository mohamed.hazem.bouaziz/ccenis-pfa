import { Component, OnInit } from '@angular/core';
import { Emploi } from 'src/app/shared/emploi.model';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-emploi',
  templateUrl: './emploi.component.html',
  styleUrls: ['./emploi.component.scss']
})
export class EmploiComponent implements OnInit {
emploi:Emploi[]
userDetails
seance;
  constructor(private userService: UserService) {
  }
  ngOnInit(): void {
    
    this.userService.getEmploi()
    .subscribe(emploi => this.emploi = emploi)
    
    this.userService.getUserProfile().subscribe(

      res => {
        this.userDetails = res["user"];
      },
      err => {
      }

    );
    this.userService.getSeance()
    .subscribe(seance => this.seance = seance)
  }

}
