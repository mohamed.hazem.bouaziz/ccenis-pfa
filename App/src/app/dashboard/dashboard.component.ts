import { Component, OnInit } from '@angular/core';
import { MenuComponent } from './menu/menu.component'
import { UserService } from '../shared/user.service';
import { Router } from '@angular/router';
import { User } from '../shared/user.model'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  userDetails;
  constructor(public userService: UserService, public router: Router) { }

  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(

      res => {
        this.userDetails = res["user"];
      },
      err => {
      }

    );

  }
  isStudent() {
    return this.userDetails.role == "Student"
  }
  isProf() {
    return this.userDetails.role == "Enseignant"
  }
  isIndus() {
    return this.userDetails.role == "industriel"
  }
}
