import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';
import { NgForm } from '@angular/forms';
import { Router, Params } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
const URL = 'http://localhost:3000/api/uploadImage';

@Component({
  selector: 'app-post-event',
  templateUrl: './post-event.component.html',
  styleUrls: ['./post-event.component.scss']
})
export class PostEventComponent implements OnInit {
  showSuccessMessage:Boolean
  serverErrorMessages:String;
  public uploader: FileUploader = new FileUploader({
    url: URL,
    itemAlias: 'image'
  });
  constructor(public eventService:EventService,public router:Router,private toastr: ToastrService) { }
  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, status: any) => {
      this.toastr.success('File successfully uploaded!');
    };
  }

  onSubmit(form:NgForm){
    this.eventService.postEvent(form.value).subscribe(
      res=>{
        this.showSuccessMessage=true;
        setTimeout(()=>this.showSuccessMessage=false,4000);
        form.resetForm();


      },
      err=>{
        if(err.status===422){
          this.serverErrorMessages=err.error.join('<br/>')
        }
        else{
          this.serverErrorMessages=err
        }

      }
    )

    }
    
}
