import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { appRoutes } from './routes';
import { UserProfileComponent } from './dashboard/user-profile/user-profile.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { UserService } from './shared/user.service';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { from } from 'rxjs';
import { AnnuaireComponent } from './annuaire/annuaire.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './dashboard/menu/menu.component';
import { EmploiComponent } from './dashboard/emploi/emploi.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { BarRatingModule } from "ngx-bar-rating";
import { EditProfileComponent } from './dashboard/edit-profile/edit-profile.component';
import { ToastrModule } from 'ngx-toastr';
import { FileSelectDirective, FileUploader, FileUploadModule } from 'ng2-file-upload';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PostEventComponent } from './post-event/post-event.component';
import { EventsCalendarComponent } from './events-calendar/events-calendar.component';
import { EventService } from './shared/event.service';
import { EventDetailsComponent } from './event-details/event-details.component';
import { AddRecommandationComponent } from './dashboard/add-recommandation/add-recommandation.component';
import { SelectUserComponent } from './dashboard/select-user/select-user.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { AddOfferComponent } from './dashboard/add-offer/add-offer.component';
import { OffresComponent } from './dashboard/offres/offres.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    SignUpComponent,
    UserProfileComponent,
    SignInComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AnnuaireComponent,
    DashboardComponent,
    MenuComponent,
    EmploiComponent,
    UserDetailsComponent,
    EditProfileComponent,
    PostEventComponent,
    EventsCalendarComponent,
    EventDetailsComponent,
    AddRecommandationComponent,
    SelectUserComponent,
    EntreprisesComponent,
    AddOfferComponent,
    OffresComponent,
    ContactComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(), // ToastrModule added,
    FileUploadModule,
    BarRatingModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, AuthGuard, UserService, EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
