import { Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { UserProfileComponent } from './dashboard/user-profile/user-profile.component';
import { AuthGuard } from './auth/auth.guard'
import { HomeComponent } from './home/home.component'
import { AnnuaireComponent } from './annuaire/annuaire.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmploiComponent } from './dashboard/emploi/emploi.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { EditProfileComponent } from './dashboard/edit-profile/edit-profile.component'
import { EventsCalendarComponent } from './events-calendar/events-calendar.component';
import { PostEventComponent } from './post-event/post-event.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { AddRecommandationComponent } from './dashboard/add-recommandation/add-recommandation.component';
import { SelectUserComponent } from './dashboard/select-user/select-user.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { AddOfferComponent } from './dashboard/add-offer/add-offer.component';
import { OffresComponent } from './dashboard/offres/offres.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
export const appRoutes: Routes = [
    {
        path: 'signup', component: UserComponent,
        children: [{ path: '', component: SignUpComponent }]
    },
    {
        path: 'home', component: HomeComponent,
    },   {
        path: 'about', component: AboutComponent,
    },{
        path: 'contact', component: ContactComponent,
    },
    {
        path: 'Entreprises', component: EntreprisesComponent
    },
    {
        path: 'events', component: EventsCalendarComponent
    },
    { path: 'eventdetails/:_id', component: EventDetailsComponent }
    ,


    {
        path: 'login', component: UserComponent,
        children: [{ path: '', component: SignInComponent }]
    },


    {
        path: 'Dashboard', component: DashboardComponent, canActivate: [AuthGuard],
        children: [{
            path: 'userprofile', component: UserProfileComponent
        }, {
            path: 'editprofile/:_id', component: EditProfileComponent
        },
        {
            path: 'addEvent', component: PostEventComponent
        },
        {
            path: 'annuaire', component: AnnuaireComponent,
            children: [{
                path: 'userdetails/:_id', component: UserDetailsComponent
            }]
        },
        {
            path: 'addrecommandation/:_id', component: AddRecommandationComponent
        },
        {
            path: 'selectUser', component: SelectUserComponent
        },
        {
            path: 'emploi', component: EmploiComponent
        },
        {
            path: 'offres', component: OffresComponent
        },
        {
            path: 'addOffer', component: AddOfferComponent
        },
        ]

    },



    {
        path: '', redirectTo: 'home', pathMatch: 'full'
    }
]
