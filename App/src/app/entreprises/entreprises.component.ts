import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-entreprises',
  templateUrl: './entreprises.component.html',
  styleUrls: ['./entreprises.component.scss']
})
export class EntreprisesComponent implements OnInit {
  userDetails;
  constructor(private userService:UserService) { }
users:User[]
  ngOnInit(): void {
    this.userService.getUsers()
    .subscribe(users => this.users = users)
  }
  getImageName(user:User) {
    return user.username + '.' + user.image.split('.')[1];
  }
}
